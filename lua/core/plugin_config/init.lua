require("core.plugin_config.lualine")
require("core.plugin_config.nvim-tree")
require("core.plugin_config.treesitter")
require("core.plugin_config.telescope")
require("core.plugin_config.lsp_config")
require("core.plugin_config.completions")
require("core.plugin_config.formatter")
require("core.plugin_config.startup")
-- require("core.plugin_config.todo_txt")
require("core.plugin_config.todo-comments")
-- [Color Scheme] : Comment the color scheme you want to use.
-- require("core.plugin_config.gruvbox")
require("core.plugin_config.darkplus")
-- [Color Scheme] : Comment the color scheme you want to use.
